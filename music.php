<!DOCTYPE html>
<html>
  <!--
  Web Programming Step by Step
  Lab #3, PHP
  -->
  <?php 
    $num_songs = 234;
    $total_hours = $num_songs/10;
    $newspages = $_GET["newspages"];
    if (!isset($newspages)){
      $newspages = 5;
    }
  ?>
  <head>
    <title>Music Viewer</title>
    <meta charset="utf-8" />
    <link href="http://www.cs.washington.edu/education/courses/cse190m/12sp/labs/3/viewer.css" type="text/css" rel="stylesheet" />
  </head>

  <body>
    
    <h1>My Music Page</h1>
    
    <!-- Exercise 1: Number of Songs (Variables) -->
    <p>
      I love music.
      I have <?=$num_songs?> total songs,
      which is over <?=$total_hours?> hours of music!
    </p>

    <!-- Exercise 2: Top Music News (Loops) -->
    <!-- Exercise 3: Query Variable -->
    <div class="section">
      <h2>Yahoo! Top Music News</h2>
    
      <ol>
        <?php
          for ($i=0; $i<$newspages; $i++){
        ?>
        <li><a href="http://music.yahoo.com/news/archive/?page=<?=$i+1?>">Page <?=$i+1?></a></li>
        <?php
        } 
        ?>
      </ol>
    </div>

    <!-- Exercise 4: Favorite Artists (Arrays) -->
    <?php 
      $favorite_artists = file("favorite.txt");   
    ?>
    <!-- Exercise 5: Favorite Artists from a File (Files) -->
    <div class="section">
      <h2>My Favorite Artists</h2>
      <ol>
      <?php
        foreach ($favorite_artists as $artist){
      ?> 
        <li><?=$artist?></li>
      <?php
        }
      ?>
      </ol>
    </div>
    
    <!-- Exercise 6: Music (Multiple Files) -->
    <!-- Exercise 7: MP3 Formatting -->
    <div class="section">
      <h2>My Music and Playlists</h2>

      <ul id="musiclist">
        <?php 
          $music_files = glob("songs/*.mp3");
          foreach($music_files as $file){ 
        ?>
        <li class="mp3item">
          <a href="<?=$file?>"><?=basename($file)?></a>
          (<?=(int)(filesize($file)/1024)?> KB)
        </li>
        <?php
          }
        ?>
        <!-- Exercise 8: Playlists (Files) -->
        <?php
          $playlists = glob("songs/*.m3u");
	  foreach ($playlists as $playlist){
        ?>
        <li class="playlistitem"><?=basename($file)?>:
          <ul>
          <?php 
           $music_files = file($playlist);
           foreach($music_files as $music_file){
	     if ($music_file[0] != "#"){ 
          ?>
            <li><?=$music_file?></li>
          <?php
             }
           }
          ?>
          </ul>
          <?php
          }
          ?>
        </li>
      </ul>
    </div>

    <div>
      <a href="https://webster.cs.washington.edu/validate-html.php">
        <img src="http://webster.cs.washington.edu/w3c-html.png" alt="Valid HTML5" />
      </a>
      <a href="https://webster.cs.washington.edu/validate-css.php">
        <img src="http://webster.cs.washington.edu/w3c-css.png" alt="Valid CSS" />
      </a>
    </div>
  </body>
</html>

